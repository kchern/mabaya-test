import logging
import socket
import os
import datetime

FORMAT = '%(asctime)s: %(levelname)s: %(message)s'

logging.basicConfig(level=logging.DEBUG, format=FORMAT)
ip_address = socket.gethostbyname(socket.gethostname())

if os.environ.get('LOGS_DB_HOST'):
    import psycopg2

    LOGS_DB_HOST = os.environ.get('LOGS_DB_HOST')
    LOGS_DB_PORT = os.environ.get('LOGS_DB_PORT')
    LOGS_DB_USER = os.environ.get('LOGS_DB_USER')
    LOGS_DB_PASSWORD = os.environ.get('LOGS_DB_PASSWORD')
    LOGS_DB_NAME = os.environ.get('LOGS_DB_NAME')

    while True:
        try:
            conn = psycopg2.connect(
                host=LOGS_DB_HOST,
                port=LOGS_DB_PORT,
                user=LOGS_DB_USER,
                password=LOGS_DB_PASSWORD, 
                dbname=LOGS_DB_NAME)
            break
        except psycopg2.OperationalError:
            logging.info('Waiting for database...')
            time.sleep(1)
    cur = conn.cursor()


def app(environ, start_response):
    request_data = environ["wsgi.input"].read()
    data = ip_address.encode('utf-8') + b';' + request_data

    logging.info('Received data: %s', request_data)
    if os.environ.get('LOGS_DB_HOST'):
        cur.execute("INSERT INTO logs (datetime, request) VALUES (%s, %s)", (datetime.datetime.now(), data.decode('utf-8')))
        conn.commit()

    start_response("200 OK", [
        ("Content-Type", "text/plain"),
        ("Content-Length", str(len(data)))
    ])

    return iter([data])
