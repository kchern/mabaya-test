# Test assignment description

## 1. Local setup contains implementation with database and data storing.
Can be started with a simple command
```
cd local
terraform init && terraform apply
```
Includes simple build of dockerized application with database. Can be tested with command like
```
curl -X POST -d "email=test@example.com" localhost:8080
```


## 2. Simple cloud implementation. 

Uses single instance (should be replaced with ASG) without storing request data to database.

Uses same instance to build project container on-the-fly, for serious projects must be replaced with some registry.

Uses Route 53 to make testing a bit easier. DNS balancing can be used to address ASG and provide requested redundancy.

Deployment similar to local one, but requires come changes in variables, like modifying hostname to use one from your Route53 and AMI

Solution uses packer to bake preconfigured AMI for faster instance/ASG startup

```
cd packer
build ubuntu-docker.json
```

Note resulting AMI id please and use is to update `infra/variables.tf`

```
cd ../infra
vim variables.tf
terraform init && terraform apply
```

## Monitoring
Assuming we have a Prometheus/Grafana setup we can use a proper alert rule to setup a p99 monitoring

```
  - alert: WebRequestLatency
    expr: (histogram_quantile(0.99, sum(rate(probe_duration_seconds{}[5m])) by (le)))  > 1
    for: 5m
    labels:
      severity: critical
    annotations:
      summary: App request latency (instance {{ $labels.instance }})
      description: "The {{ $labels.job }} {{ $labels.route }} is experiencing {{ printf \"%.2f\" $value }}s 99th percentile latency\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
```