resource "aws_vpc" "test-vpc" {
  # Amoeba-level VPC definition
  cidr_block = "10.0.0.0/16"

  tags = {
    env   = "MabayaTest"
    Owner = "Kyrylo Chernousov"
  }
}

resource "aws_subnet" "test-public-subnet" {
  # Amoeba-level public subnet definition
  vpc_id     = aws_vpc.test-vpc.id
  cidr_block = "10.0.1.0/24"

  tags = {
    env   = "MabayaTest"
    Owner = "Kyrylo Chernousov"
  }
}


resource "aws_instance" "test-server1" {
  # Brutally hardcoded instance definition
  subnet_id     = aws_subnet.test-public-subnet.id
  instance_type = "t2.micro"
  ami           = var.ami_id
  key_name      = "kc-cprime-keypair"
  associate_public_ip_address = true
  vpc_security_group_ids = [aws_security_group.test-sg.id]

  user_data = <<EOF
#!/bin/bash
set -x
sudo apt-get update
sudo apt-get install -y git
git clone ${var.repo} app
cd app/${var.path}
docker buildx build -t ${var.application} .
docker run --restart unless-stopped -d -p 0.0.0.0:8080:8080 ${var.application}
  EOF

  tags = {
    env   = "MabayaTest"
    Owner = "Kyrylo Chernousov"
  }
}

data "aws_route53_zone" "test-zone" {
  # Existing zone definition
  name         = var.domain_name
}

resource "aws_route53_record" "test-record" {
  # Test record definition
  zone_id = data.aws_route53_zone.test-zone.id
  name    = "test."
  type    = "A"
  ttl     = "300"
  records = [aws_instance.test-server1.public_ip]
}

resource "aws_security_group" "test-sg" {
  # Test security group definition
  name        = "test-sg"
  description = "Allow inbound traffic"
  vpc_id      = aws_vpc.test-vpc.id

  ingress {
    description = "HTTP"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    }
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }
}

resource "aws_internet_gateway" "test-igw" {
  # Test internet gateway definition
  vpc_id = aws_vpc.test-vpc.id
}

resource "aws_default_route_table" "test-default-route-table" {
  # Test default route table definition
  default_route_table_id = aws_vpc.test-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.test-igw.id
  }
}
