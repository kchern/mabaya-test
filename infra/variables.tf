# variable "tags" {
#   description = "Default tags to apply to all resources."
#   type        = map(any)
#   default = {
#     Owner = "Kyrylo Chernousov"
#     env   = "Development"
#   }
# }

variable repo {
  description = "The name of the repository to use."
  type        = string
  default     = "https://gitlab.com/kchern/mabaya-test"
}

variable path {
  description = "The path to app files."
  type        = string
  default     = "app"
}

variable application {
  description = "The filename of the application."
  type        = string
  default     = "app.py"
}

variable ami_id {
  description = "The AMI ID to use for the instance."
  type        = string
  default     = "ami-01395c5669c2ed437" # Ubuntu 22.04 with docker installed
}

variable "domain_name" {
  description = "The domain name to use for the instance."
  type        = string
  default     = "mytestdmain.cf"
}
