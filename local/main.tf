terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.23.1"
    }
  }
}

provider "docker" {
  host = "unix:///var/run/docker.sock"
}

resource "docker_image" "testapp" {
  name  = "example"

  build {
    path = "../app"
    tag  = ["app:local"]
    build_arg = {
      env : "local"
    }
    label = {
      author : "Kyrylo Chernousov"
    }
  }

  triggers = {
    dir_sha1 = sha1(join("", [for f in fileset(path.module, "../app/*") : filesha1(f)]))
  }
}

resource "docker_network" "testapp" {
  name = "testapp"
}

resource "docker_container" "testapp" {
  name = "testapp"
  image = docker_image.testapp.image_id
  networks_advanced {
    name = docker_network.testapp.name
  } 

  ports {
    internal = 8080
    external = 8080
  }

  depends_on = [
    docker_container.database
  ]

  env = [
    "LOGS_DB_HOST=database",
    "LOGS_DB_PORT=5432",
    "LOGS_DB_USER=postgres",
    "LOGS_DB_PASSWORD=postgres",
    "LOGS_DB_NAME=postgres"
  ]

}

resource "docker_container" "database" {
  name = "database"
  image = "postgres:13.3"
  networks_advanced {
    name = docker_network.testapp.name
  }

  ports {
    internal = 5432
    external = 5432
  }

  volumes {
    container_path = "/docker-entrypoint-initdb.d/init.sql"
    host_path      = "${abspath(path.module)}/pg_init.sql"
    read_only      = true

  }

  env = [ 
    "POSTGRES_NAME=postgres",
    "POSTGRES_USER=postgres",
    "POSTGRES_PASSWORD=postgres"
  ]
  
}
